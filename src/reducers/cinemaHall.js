import {
  BUY_TICKETS,
  CHOOSE_PLACE, COUNT_TICKETS_SUM, CREATE_HALL, REMOVE_TICKETS_FROM_LIST, SHOW_TICKETS_LIST,
} from "../actions";

const initialState = {
  loaded: false,
  hallOne: [
    [1, 1, 1, 1],
    [1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1],
    [1, 2, 2, 2, 2, 2, 1],
    [1, 2, 2, 2, 2, 2, 1],
    [1, 1, 0, 2, 2, 2, 2, 0, 1, 1],
    [3, 3, 0, 3, 3, 0, 3, 3]
  ],
  hallTwo: [
    [0, 1, 1, 0],
    [1, 1, 1, 1],
    [1, 1, 1, 1],
    [1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1],
    [1, 1, 2, 2, 2, 1, 1],
    [1, 1, 2, 2, 2, 1, 1],
    [1, 1, 2, 2, 2, 1, 1],
    [3, 3, 0, 0, 0, 0, 0, 0, 0, 3, 3]
  ],
  hallThree: [
    [1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1],
    [1, 1, 2, 2, 2, 1, 1],
    [1, 1, 2, 2, 2, 1, 1],
    [3, 3, 0, 0, 0, 3, 3]
  ],
  currentHall: [],
  ticketsList: [],
  ticketsSum: 0
};

const cinemaHall = (state = initialState, action) => {
  switch (action.type){

    case CREATE_HALL:
      return{
        ...state,
        currentHall: action.payload,
        ticketsList: [],
        loaded: true
      };
    case CHOOSE_PLACE:
      return{
        ...state,
        currentHall: action.payload
      };
    case COUNT_TICKETS_SUM:
      return{
        ...state,
        ticketsSum: action.payload
      };
    case SHOW_TICKETS_LIST:
      return{
        ...state,
        ticketsList: action.payload
      };
    case REMOVE_TICKETS_FROM_LIST:
      return{
        ...state,
        currentHall: action.payload
      };
    case BUY_TICKETS:
      return{
        ...state,
        currentHall: action.payload
      };

    default:
      return state;
  }
};

export default cinemaHall;
