import { combineReducers } from "redux";

import cinemaHall from './cinemaHall';

const reducers = combineReducers({
  cinemaHall
});

export default reducers;
