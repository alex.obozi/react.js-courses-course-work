export const CHOOSE_PLACE = 'CHOOSE_PLACE';
export const CREATE_HALL = 'CREATE_HALL';
export const COUNT_TICKETS_SUM = 'COUNT_TICKETS_SUM';
export const SHOW_TICKETS_LIST = 'SHOW_TICKETS_LIST';
export const REMOVE_TICKETS_FROM_LIST = 'REMOVE_TICKETS_FROM_LIST';
export const BUY_TICKETS = 'BUY_TICKETS';

export const choosePlace = (place) => (dispatch) => {
  dispatch({
    type: CHOOSE_PLACE,
    payload: place
  });
};
export const createHall = (hall) => (dispatch) => {
  dispatch({
    type: CREATE_HALL,
    payload: hall
  });
};
export const countTicketsSum = (sum) => (dispatch) => {
  dispatch({
    type: COUNT_TICKETS_SUM,
    payload: sum
  });
};
export const showTicketsList = (list) => (dispatch) => {
  dispatch({
    type: SHOW_TICKETS_LIST,
    payload: list
  });
};
export const removeTicketsFromList = (list) => (dispatch) => {
  dispatch({
    type: REMOVE_TICKETS_FROM_LIST,
    payload: list
  });
};
export const buyTickets = (list) => (dispatch) => {
  dispatch({
    type: BUY_TICKETS,
    payload: list
  });
};

