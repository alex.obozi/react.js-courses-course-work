import React from 'react';
import {Link} from "react-router-dom";

const ChooseHall = () => {
  return (
    <>
      <h1>Choose wanted cinema hall</h1>
      <div className='choose-hall'>
        <Link to='/hall/1'>First hall</Link>
        <Link to='/hall/2'>Second hall</Link>
        <Link to='/hall/3'>Third hall</Link>
      </div>
    </>
  );
};

export default ChooseHall;
