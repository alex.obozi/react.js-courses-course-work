import React, {Component} from 'react';
import Tickets from "./Tickets";
import CinemaHall from "./CinemaHall";

class HallContainer extends Component {
  render() {
    return (
      <div className="hall-container">

        <h1>Hall #{this.props.match.params.hallid}</h1>

        <div className="hall-block">
          <CinemaHall hallId={this.props.match.params.hallid}/>
          <Tickets hallId={this.props.match.params.hallid}/>
        </div>
      </div>
    );
  }
}

export default HallContainer;
