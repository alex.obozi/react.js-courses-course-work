import React, {Component} from 'react';
import {connect} from "react-redux";
import {countTicketsSum, removeTicketsFromList, showTicketsList, buyTickets} from "../actions";


class Tickets extends Component {

  componentDidUpdate(prevProps, prevState, snapshot) {

  }

  showTicketsSum = () => {
    let sum = 0;
    this.props.ticketsList.map(ticket => sum = sum + ticket.cost);
    this.props.countTicketsSum(sum)

  };

  removeTickets = (id) => {

    let changedArray = this.props.currentHall.map((row) => {
      row.map((col) => {
        if(col.id === id){
          col.checked = false;
          return col
        }
        return col
      });
      return row
    });

    this.props.removeTicketsFromList(changedArray);

    let filteredList = [];

    this.props.currentHall.forEach(row => {
      row.forEach(place => {
        if(place.checked === true){
          filteredList = [...filteredList, place]
        }
      });
    });
    this.props.showTicketsList(filteredList);

  };

  buyTickets = () => {
    let changedArray = this.props.currentHall.map((row) => {
      row.map((col) => {
        if(col.checked === true){
          col.bought = true;
          col.checked = false;
          return col
        }
        return col
      });
      return row
    });

    localStorage.setItem('hall' + this.props.hallId, JSON.stringify(changedArray));

    this.props.buyTickets(changedArray);
    this.props.showTicketsList([]);

    let currentTime = new Date();
    let targetTime = new Date(currentTime.getTime() + 60000);
    localStorage.setItem('targetTime', JSON.stringify(targetTime));
  };

  render() {
    const {ticketsList, ticketsSum} = this.props;
    const {showTicketsSum, removeTickets, buyTickets} = this;
    showTicketsSum();
    return (
      <div className="tickets">
        <h3>Tickets:</h3>
        <ul>
          {
            ticketsList.map(ticket =>
              <li key={ticket.id}>
                <div className='ticket-info'>
                  <p>Row: {ticket.row}</p>
                  <p>Place: {ticket.place}</p>
                  <p>Cost: {ticket.cost}₴</p>
                </div>
                <button onClick={() => removeTickets(ticket.id)}>Remove</button>
              </li>
            )
          }

        </ul>

        <div className="buy-block">
          <p>Summary: {ticketsSum}₴</p>
          {
            ticketsSum ? <button onClick={buyTickets}>Buy</button> : ''
          }

        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  currentHall: state.cinemaHall.currentHall,
  ticketsSum: state.cinemaHall.ticketsSum,
  ticketsList: state.cinemaHall.ticketsList
});
const mapDispatchToProps = (dispatch) => ({
  countTicketsSum: (sum) => {
    dispatch(countTicketsSum(sum))
  },
  removeTicketsFromList: (changedArray) => {
    dispatch(removeTicketsFromList(changedArray))
  },
  showTicketsList: (list) => {
    dispatch(showTicketsList(list))
  },
  buyTickets: (list) => {
    dispatch(buyTickets(list))
  },

});

export default connect(mapStateToProps, mapDispatchToProps)(Tickets);
