import React from 'react';
import './App.scss';

import { Link, Route } from 'react-router-dom';
import HallContainer from "./HallContainer";
import ChooseHall from "./ChooseHall";

class App extends React.Component {

  render(){

    return (
      <div className="App">
        <header>
          <Link to='/'>Home</Link>
        </header>


        <Route exact path='/' component={ChooseHall} />
        <Route exact path='/hall/:hallid' component={HallContainer}/>

      </div>
    );

  }
}

export default App;
