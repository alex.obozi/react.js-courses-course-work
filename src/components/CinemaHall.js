import React from 'react';
import {connect} from 'react-redux';

import {choosePlace, showTicketsList} from "../actions";
import {createHall} from "../actions";



class CinemaHall extends React.Component {


  componentDidMount() {

    let hallNumber;
    let hallInitial;

    switch (this.props.hallId.toString()) {
      case '1':
        hallNumber = this.checkHall(this.props.hallId, this.props.hallOne);
        hallInitial = this.props.hallOne;
        break;
      case '2':
        hallNumber = this.checkHall(this.props.hallId, this.props.hallTwo);
        hallInitial = this.props.hallTwo;
        break;
      case '3':
        hallNumber = this.checkHall(this.props.hallId, this.props.hallThree);
        hallInitial = this.props.hallThree;
        break;
      default:
        break;
    }

    this.props.createHall(hallNumber);

    let currentTime = new Date().toISOString();
    let targetTime = JSON.parse(localStorage.getItem('targetTime'));

    if(currentTime > targetTime){

      let generateHall = this.initHall(hallInitial);
      localStorage.setItem('hall' + this.props.hallId, JSON.stringify(generateHall));
      this.props.createHall(generateHall);

    }
  }

  checkHall = (hallNumber, object) => {
    if (localStorage.getItem('hall' + hallNumber) !== null) {
      return  JSON.parse(localStorage.getItem('hall' + hallNumber));
    } else {
      let generateHall = this.initHall(object);
      localStorage.setItem('hall' + hallNumber, JSON.stringify(generateHall));
      return generateHall;
    }
  };

  initHall = (array) => {

    let res = [];
    let counter = 0;

    array.map((row, i) => {

      let newHall = [];
      let place = 0;

      row.map((col, j) => {

        switch (col.toString()) {
          case '0':
            newHall = [...newHall, {id: ++counter, type: col}];
            break;
          case '1':
            newHall = [...newHall, {id: ++counter, type: col, place: ++place, cost: 50, bought: false, checked: false, row: i+1}];
            break;
          case '2':
            newHall = [...newHall, {id: ++counter, type: col, place: ++place, cost: 80, bought: false, checked: false,  row: i+1}];
            break;
          case '3':
            newHall = [...newHall, {id: ++counter, type: col, place: ++place, cost: 100, bought: false, checked: false,  row: i+1}];
            break;
          default:
            break;
        }

        return newHall;

      });

      res = [...res, newHall];
      return res

    });

    return res;

  };

  choosePlace = (id) => {
    let changedArray = this.props.currentHall.map((row) => {
        row.map((col) => {
          if(col.id === id){
            col.checked = !col.checked;
            return col
          }
          return col
        });
        return row
    });

    this.props.choosePlace(changedArray);

    this.showTickets();
  };

  showTickets = () => {
    let filteredList = [];

    this.props.currentHall.forEach(row => {
      row.forEach(place => {
        if(place.checked === true){
          filteredList = [...filteredList, place]
        }
      });
    });
    this.props.showTicketsList(filteredList);
  };

  render() {

    const {currentHall, loaded} = this.props;
    const {choosePlace} = this;
    return (
      <div className="hall">

        {
          loaded ?
            currentHall.map((row, i) => (
                <div className="row" key={i}>
                  {row.map((col, j) => (
                    <span onClick={() => choosePlace(col.id)} className={`place type-${col.type} ${col.checked ? 'chosen' : '' } ${col.bought ? 'bought' : ''}`} key={j}>{col.place}</span>
                  ))}
                </div>
            ))
            :
            <p>Loading...</p>
        }

      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  currentHall: state.cinemaHall.currentHall,
  hallOne: state.cinemaHall.hallOne,
  hallTwo: state.cinemaHall.hallTwo,
  hallThree: state.cinemaHall.hallThree,
  loaded: state.cinemaHall.loaded,
  ticketsSum: state.cinemaHall.ticketsSum,
  ticketsList: state.cinemaHall.ticketsList
});

const mapDispatchToProps = (dispatch) => ({
  choosePlace: (changedArray) => {
    dispatch(choosePlace(changedArray))
  },
  createHall: (hall) => {
    dispatch(createHall(hall))
  },
  showTicketsList: (list) => {
    dispatch(showTicketsList(list))
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(CinemaHall);
