import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/Wrapper';

ReactDOM.render(<App />, document.getElementById('root'));
